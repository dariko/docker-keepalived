FROM debian:buster

RUN set -x;apt-get update \
    && apt install -y keepalived curl python-pip \
    && pip install jinja2-cli \
    && rm -rf /var/lib/apt/lists

ADD etc/keepalived.conf.j2.sample /etc/keepalived.conf.j2.sample
ADD run.sh    /
CMD /run.sh
