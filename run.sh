#!/bin/bash -x

KEEPALIVED_CONFIG_TEMPLATE_FILE=${KEEPALIVED_CONFIG_TEMPLATE_FILE:-"/etc/keepalived.conf.j2.sample"}

if [ "x$KEEPALIVED_CONFIG_TEMPLATE" != "x" ]; then
    echo "$KEEPALIVED_CONFIG_TEMPLATE" > /etc/keepalived.conf.j2.env
    KEEPALIVED_CONFIG_TEMPLATE_FILE=/etc/keepalived.conf.j2.env
fi

if [ "x$KEEPALIVED_CONFIG_TEMPLATE_FILE" != x ];then
    jinja2 "$KEEPALIVED_CONFIG_TEMPLATE_FILE" > /etc/keepalived.conf
fi

exec keepalived --use-file /etc/keepalived.conf \
           --dont-fork \
           --log-console \
           --vrrp \
           --dump-conf
