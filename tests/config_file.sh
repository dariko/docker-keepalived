#!/bin/sh -x

set -e

. $(dirname "$0")/common.sh

cat > $tmpdir/left.conf <<EOF
vrrp_instance vip_1 {
    interface eth0

    virtual_router_id 50
    priority 50
    state BACKUP

    #unicast_src_ip 172.31.23.1
    #unicast_peer {
        #172.31.23.2
    #}
    virtual_ipaddress {
         172.31.23.3/32
    }
}
EOF

cat > $tmpdir/right.conf <<EOF
vrrp_instance vip_1 {
    interface eth0

    virtual_router_id 50
    priority 100
    state BACKUP

    #unicast_src_ip 172.31.23.2
    #unicast_peer {
        #172.31.23.1
    #}
    virtual_ipaddress {
         172.31.23.3/32
    }
}
EOF

docker cp $tmpdir/left.conf left:/left.conf
docker cp $tmpdir/right.conf right:/right.conf

docker exec -td \
    -e KEEPALIVED_CONFIG_TEMPLATE_FILE=/left.conf \
    left \
    /run.sh

docker exec -td \
    -e KEEPALIVED_CONFIG_TEMPLATE_FILE=/right.conf \
    right \
    /run.sh

sleep 6
docker logs left
docker logs right

# verify vip is on right
container_has_ip right 172.31.23.3
! container_has_ip left 172.31.23.3

# kill right
docker kill right

sleep 6
docker logs left
docker logs right

# verify vip is on left
container_has_ip left 172.31.23.3
