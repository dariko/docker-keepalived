#!/bin/sh -x

set -e

. $(dirname "$0")/common.sh

cat > $tmpdir/template.conf <<EOF
vrrp_instance vip_1 {
    interface eth0

    virtual_router_id 50
    priority {{environ('priority')}}
    state BACKUP

    virtual_ipaddress {
{% for vip in environ('vips').split(',') %}
        {{ vip }}
{% endfor %}
         172.31.23.3/32
    }
}
EOF

docker cp $tmpdir/template.conf left:/template.conf.j2
docker cp $tmpdir/template.conf right:/template.conf.j2

docker exec -td \
    -e priority=50 \
    -e vips=172.31.23.3,172.31.23.4 \
    -e KEEPALIVED_CONFIG_TEMPLATE_FILE=/template.conf.j2 \
    left \
    /run.sh

docker exec -td \
    -e priority=100 \
    -e vips=172.31.23.3,172.31.23.4 \
    -e KEEPALIVED_CONFIG_TEMPLATE_FILE=/template.conf.j2 \
    right \
    /run.sh

sleep 6

docker logs left
docker logs right

container_has_ip right 172.31.23.3
! container_has_ip left 172.31.23.3
