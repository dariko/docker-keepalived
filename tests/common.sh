#cat test_image.img | docker load
docker network create test

tmpdir=$(mktemp -d)

function container_has_ip {
    container=$1
    ip=$2
    docker exec $container bash -c "ip a show | grep -q $ip/32"
}

docker run -dt --name left \
           --network test \
           --privileged \
           $CI_COMMIT_IMAGE \
           sleep 600

docker run -dt --name right \
           --network test \
           --privileged \
           $CI_COMMIT_IMAGE \
           sleep 600

docker logs left
docker logs right
