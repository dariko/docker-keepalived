#!/bin/bash -x

export CI_COMMIT_IMAGE=local-image
export CI_COMMIT_IMAGE_DIR=/tmp/dk
mkdir -p $CI_COMMIT_IMAGE_DIR

docker rm -f test_container


if [ "$1" == "-s" ]
then
    shift
else
    docker build -t $CI_COMMIT_IMAGE .
    docker image save $CI_COMMIT_IMAGE > "$CI_COMMIT_IMAGE_DIR/cache"
fi

docker run -d --name test_container \
    --privileged \
    -e CI_COMMIT_IMAGE=$CI_COMMIT_IMAGE \
    -v $CI_COMMIT_IMAGE_DIR:/image \
    -v $(pwd)/tests:/tests \
    docker:dind

docker exec -it test_container sh -c "
    cat /image/cache | docker load
"

docker exec -it test_container $1
result=$?
#docker rm -f test_container

if [ $result -ne 0 ];then
    echo "failed"
    exit 1
fi
